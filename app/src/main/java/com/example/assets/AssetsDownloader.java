package com.example.assets;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

class AssetsDownloader extends AsyncTask<String, Void, File> {

    private final Context mContext;
    private final Callback mCallback;

    public AssetsDownloader(Context mContext, Callback mCallback) {
        this.mContext = mContext;
        this.mCallback = mCallback;
    }

    @Override
    protected File doInBackground(String... strings) {
        File outputFile = new File(mContext.getFilesDir(), strings[0]);
        InputStream inputStream = null;
        OutputStream outputStream = null;


        try {
            inputStream = mContext.getAssets().open(strings[0]);
            if (outputFile.exists() && outputFile.length() == inputStream.available()) {
                return outputFile;
            } else {
                outputFile.delete();
            }

            outputStream = new FileOutputStream(outputFile);

            byte[] buffer = new byte[4096];
            int len;
            while ((len = inputStream.read(buffer)) != -1) {
                outputStream.write(buffer, 0, len);
            }
        } catch (IOException exception) {
            Log.wtf("!!!", exception);
        }
        finally {
            try {
                if (inputStream != null) {
                    inputStream.close();
                }
                if (outputStream != null) {
                    outputStream.close();
                }
            } catch (IOException exception) {
                Log.wtf("!!!", exception);
            }
        }

        return outputFile;
    }

    @Override
    protected void onPostExecute(File file) {
        super.onPostExecute(file);
        mCallback.onComplete(file);
    }

    interface Callback {

        void onComplete(File filew);
    }
}
